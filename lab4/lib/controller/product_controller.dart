import '../model/product.dart';

class ProductController{
  final List<Product> _currentProduct = <Product>[];


  int addProduct(Product product) {
    _currentProduct.add(product);
    return 0;
  }

  void deleteProduct(Product product) {
    _currentProduct.remove(product);

  }
}