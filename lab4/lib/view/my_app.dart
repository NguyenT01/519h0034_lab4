import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab4/repository/memory_repository.dart';
import 'package:lab4/view/product_category_screen.dart';
import 'package:provider/provider.dart';

import '../repository/repository.dart';
import './cart_screen.dart';
import './screen_export.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<MemoryRepository>(
          create: (_)=> MemoryRepository(),
          lazy: false,
        ),
      ],
      child: MaterialApp(
        routes: {
          '/' : (context) => LoginScreen(),
          '/home': (context)=> HomePage(),
          '/search': (context) => SearchScreen(),
          '/cart': (context)=>CartScreen(),
          '/p_detail': (context) => ProductDetail(),
          '/category': (context)=> CategoryScreen(),
          '/pcate': (context)=> ProductCategoryScreen(),
        },
        initialRoute: '/',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.teal,
          brightness: Brightness.light,
          //colorScheme: ColorScheme()
          appBarTheme: AppBarTheme(
            systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.teal,),
          )

        ),
      ),
    );
  }
}
