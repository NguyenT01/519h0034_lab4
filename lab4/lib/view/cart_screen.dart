import 'package:flutter/material.dart';
import 'package:lab4/model/model_export.dart';
import 'package:lab4/repository/memory_repository.dart';
import 'package:provider/provider.dart';

import '../repository/repository.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    List<Product> data = [];

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
      ),
      body: Consumer<MemoryRepository>(
        builder: (context, repository, child) {
          data = repository.findAllProducts();
          if (data.length > 0) {
            return Container(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      'Items in cart (' + data.length.toString() + ')',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Expanded(
                    flex: 20,
                    child: ListView.separated(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: false,
                        separatorBuilder: (context, int) => SizedBox(
                              height: 20,
                            ),
                        itemCount: data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return productCard(
                              data[index].image,
                              data[index].title!.length > 50
                                  ? data[index].title!.substring(0, 35)
                                  : data[index].title,
                              data[index].price.toString(),
                              data[index].id.toString(),
                              data[index],
                              repository);
                        }),
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: Container(
                padding: EdgeInsets.all(32),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Your cart is empty.',
                      style: TextStyle(fontSize: 18),
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 32,
                      child: ElevatedButton(
                        child: Text('Return to Home Screen'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    ));
  }

  Widget productCard(var imgUrl, var textTitle, var textPrice, var id, var json,
      var repository) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/p_detail', arguments: [id, textTitle]);
      },
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.network(
                        imgUrl,
                        fit: BoxFit.fill,
                        height: 80,
                      ),
                      //const SizedBox(width: 16,),
                      Text(
                        '\$ ' + textPrice,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                            color: Colors.red),
                      )
                    ]),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                textTitle,
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        repository.deleteProduct(json);
                      },
                      child: Text('REMOVE')),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
