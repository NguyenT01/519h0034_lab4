import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab4/service/product_home_api.dart';

import 'dart:math' as math;


class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.grey,
          statusBarBrightness: Brightness.light,
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Category',
                style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 16,
              ),
              _buildCardCategory(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCardCategory() {
    return FutureBuilder(
      future: ProductHomeApi.getCategoryAll(),
        builder: (context, AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
      if (snapshot.connectionState == ConnectionState.done) {
        if (snapshot.hasData) {
          final List<Map<String, dynamic>>? cateData = snapshot.data;
          return Expanded(
            child: ListView.separated(
              separatorBuilder:  (context, index){
                return SizedBox(height: 24,);
              },
              itemCount: cateData!.length,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return
                  Expanded(
                      child: Card(
                    elevation: 8,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    color: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
                    child: SizedBox(
                      child: InkWell(
                        child: Container(
                          padding: EdgeInsets.all(16),
                          child: Row(children: [
                            Expanded(
                              child: Text(
                                cateData[index]['cname'].toUpperCase(),
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              flex: 3,
                            ),
                            Expanded(
                              child: ClipRRect(
                                child: Image.network(cateData[index]['cimg'], height: 100, width: 100,),
                                borderRadius: BorderRadius.circular(16),
                              ),
                              flex: 1,
                            ),
                          ]),
                        ),
                        onTap: () {
                          Navigator.pushNamed(context, '/pcate',arguments: cateData[index]['cname']);
                        },
                      ),
                      height: 150,
                    ),
                  ));

              }
            ),
          );
        }
        else{
          return Text('No Data');
        }
      }
      else{
        return Container(child: Center(
          child: CircularProgressIndicator(),
        ),);
      }
    });
  }
}
