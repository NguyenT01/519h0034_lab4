import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../service/product_home_api.dart';

class ProductCategoryScreen extends StatefulWidget {
  const ProductCategoryScreen({Key? key}) : super(key: key);

  @override
  State<ProductCategoryScreen> createState() => _ProductCategoryScreenState();
}

class _ProductCategoryScreenState extends State<ProductCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as String;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.grey,
          statusBarBrightness: Brightness.light,
        ),
        title: Text(args),
      ),
      body: SafeArea(
        child: FutureBuilder<dynamic>(
          future: ProductHomeApi.getProductByCate(args),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              print("---> "+snapshot.toString());
              if(snapshot.hasData){
                final data = snapshot.data;
                return GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, mainAxisSpacing: 12, crossAxisSpacing: 12),
                    itemCount: data!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return productCard(data[index]['image'], data[index]['title'].length > 50 ? data[index]['title'].substring(0,35) : data[index]['title'],
                          data[index]['price'].toString(), data[index]['id'].toString());
                    });
              }
              else{
                return Container(child: Text('No data'),);
              }
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      const SizedBox(height: 18),
                      Text('Đang tải dữ liệu, vui lòng chờ'),
                    ],
                  ));
            } else {
              return Center(child: Text('Đã có lỗi xảy ra, vui lòng thử lại'));
            }
          },
        ),
      ),
    );
  }


  Widget productCard(var imgUrl, var textTitle, var textPrice, var id) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, '/p_detail', arguments: [id,textTitle]);
      },
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.network(
                  imgUrl,
                  fit: BoxFit.fill,
                  height: 80,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                textTitle,
                style: TextStyle(fontSize: 12,),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                '\$ '+textPrice,
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 16, color: Colors.red),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
