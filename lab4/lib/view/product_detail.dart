import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:lab4/repository/memory_repository.dart';
import 'package:provider/provider.dart';

import '../model/model_export.dart';
import '../repository/repository.dart';

class ProductDetail extends StatefulWidget {
  const ProductDetail({Key? key}) : super(key: key);

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as List;

    final repository = Provider.of<MemoryRepository>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: Text(args[1]),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: FutureBuilder<dynamic>(
            future: http
                .get(Uri.parse('https://fakestoreapi.com/products/' + args[0])),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: Column(
                    children: [
                      CircularProgressIndicator(),
                      const SizedBox(height: 16),
                      Text('Đang tải dữ liệu')
                    ],
                  ),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData) {
                  var data = json.decode(snapshot.data.body);
                  return Column(children: [
                    Expanded(
                      child: ListView(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              productImg(data['image']),
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                data['title'],
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              const SizedBox(height: 12,),
                              Text(
                                '\$ '+data['price'].toString(),
                                style: TextStyle(
                                    fontSize: 24,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.start,
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              titleProductDetailBar('DESCRIPTION'),
                              const SizedBox(
                                height: 12,
                              ),
                              Text(
                                data['description'],
                                style: TextStyle(fontSize: 14),
                              ),
                              const SizedBox(height: 16,),
                              titleProductDetailBar('RATING SCORE'),
                              const SizedBox(height: 16,),
                              ratingField(Rating.fromJson(data['rating'])),
                            ],
                          ),
                        ],
                        scrollDirection: Axis.vertical,
                      ),
                    ),
                    ElevatedButton(
                      child: SizedBox(
                        width: double.infinity,
                        child: Text(
                          'ADD TO CART',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      onPressed: () {
                        repository.addProduct(Product.fromJson(data));
                        //print("----->"+repository.getLength().toString());
                        Fluttertoast.showToast(
                          msg: 'Add an item successfully',
                          toastLength: Toast.LENGTH_SHORT,
                          fontSize: 16,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: Colors.grey,
                          textColor: Colors.white,
                        );
                      },
                    ),
                  ]);
                }
              }

              return Center(
                child: Column(
                  children: [Text('Đã có lỗi xảy ra')],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget ratingField(Rating rating){
    return Row(
      children: [
        Expanded(
          child: Center(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.star, color: Colors.yellow,size: 48),
                    Text(rating.rate.toString(), style: TextStyle(fontSize: 48, fontWeight: FontWeight.bold),)
                  ],
                ),
                Text(rating.count.toString()+' reviews total', style: TextStyle(fontSize: 16),),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget titleProductDetailBar(String txt){
    return Row(
      children: [
        Expanded(
          child: Container(
            child: Text(
              txt,
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white),
            ),
            padding: EdgeInsets.all(8),
            color: Colors.grey,
          ),
        )
      ],
    );
  }

  Widget productImg(String img) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: Image.network(
        img,
        height: 400,
      ),
    );
  }
}
