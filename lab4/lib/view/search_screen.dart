import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../repository/memory_repository.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final formKey = GlobalKey<FormState>();
  final searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // final repository = Provider.of<MemoryRepository>(context, listen: false);
    // print(">>>>>"+repository.getLength().toString());

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(
          'Search',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 16),
        padding: EdgeInsets.only(left: 32, right: 32),
        child: Column(
          children: [
            searchHeader(),
          ],
        ),
      ),
    ));
  }

  Widget searchHeader() {
    return Form(
      key: formKey,
      child: Row(
        children: [
          searchForm(),
        ],
      ),
    );
  }

  void validate(){
    final form = formKey.currentState;
    if(!form!.validate()) return;
    else{
      var sData = searchController.text;
    }
  }

  Widget searchForm() {
    return Expanded(
      child: TextFormField(
        decoration: InputDecoration(
          hintText: 'Search anything here',
          border: OutlineInputBorder(
              borderSide: BorderSide(), borderRadius: BorderRadius.circular(8)),
          suffixIcon: IconButton(
            icon: Icon(Icons.search),
            onPressed: validate,
          ),
        ),
        controller: searchController,

      ),
      flex: 3,
    );
  }
}
