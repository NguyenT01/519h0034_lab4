import 'dart:async';
import 'dart:collection';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../model/model_export.dart';

class ProductHomeApi {
  //static const urlRaw = 'https://fakestoreapi.com/products';

  Future<List<Map>> getProductsAll() async {
    var url = Uri.parse('https://fakestoreapi.com/products');
    var response = await http.get(url);

    var jsonObject = json.decode(response.body);

    return jsonObject;
  }

  static Future<List<Map<String, dynamic>>> getCategoryAll() async {
    var response = await http
        .get(Uri.parse('https://fakestoreapi.com/products/categories'));

    var jsonObject = json.decode(response.body);

    List<Map<String, dynamic>> categoryJs = [];
    Map<String, String> imgCate = {
      'je': 'https://fakestoreapi.com/img/71pWzhdJNwL._AC_UL640_QL65_ML3_.jpg',
      'el': 'https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg',
      'mc': 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
      'wc': 'https://fakestoreapi.com/img/61pHAEJ4NML._AC_UX679_.jpg'
    };

    jsonObject.forEach((element) {
      if (element == 'electronics')
        categoryJs
            .add({'cname': element, 'cimg': imgCate['el'], 'color': 'green'});
      else if (element == 'jewelery')
        categoryJs
            .add({'cname': element, 'cimg': imgCate['je'], 'color': 'red'});
      else if (element == "men's clothing")
        categoryJs
            .add({'cname': element, 'cimg': imgCate['mc'], 'color': 'purple'});
      else
        categoryJs
            .add({'cname': element, 'cimg': imgCate['wc'], 'color': 'blue'});
    });

    return categoryJs;
  }

  static Future<List<dynamic>> getProductByCate(String cate) async {
    var response = await http
        .get(Uri.parse('https://fakestoreapi.com/products/category/' + cate));

    var jsonObject = json.decode(response.body);
    //print(jsonObject);
    return jsonObject;
  }
}
