import 'package:cloud_firestore/cloud_firestore.dart';
import '../user_profile.dart';
import 'dart:async';

class UserProfileDao {
  final CollectionReference collection = FirebaseFirestore.instance.collection('user_profile');
  Stream userStream = FirebaseFirestore.instance.collection('user_profile').doc('user1').snapshots();

  Future<void> updateUser(UserProfile userProfile){
      return collection.doc('user1').update(userProfile.toJson())
          .then((value) => print("User updated"))
          .catchError((onError) =>"Failed to update user $onError");
  }




}