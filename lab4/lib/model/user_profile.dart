import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfile{
  String name;
  String nickname;
  String age;
  String gender;

  DocumentReference? documentReference;

  UserProfile({required this.name, required this.nickname, required this.age, required this.gender, this.documentReference});

  factory UserProfile.fromJson(Map<dynamic,dynamic> json)=> UserProfile(name: json['name'], nickname: json['nickname'], age: json['age'], gender:  json['gender']);

  Map<String, dynamic> toJson()=> <String,dynamic>{
    'name': name,
    'nickname': nickname,
    'age': age,
    'gender': gender,
  };

  //TODO Add snapshot
  factory UserProfile.fromSnapshot(DocumentSnapshot snapshot){
    final user_profile = UserProfile.fromJson(snapshot.data() as Map<String, dynamic>);
    user_profile.documentReference = snapshot.reference;
    return user_profile;
  }


}