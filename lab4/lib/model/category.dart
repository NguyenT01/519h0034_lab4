
class Category{
  String? cname;
  String? img;

  Category({this.cname, this.img});

  factory Category.fromJson(Map<String, dynamic> json)=> Category(
    cname: json['cname'],
    img: json['img'],
  );

  Map<String,dynamic> toJson()=>{
    'cname': cname,
    'img': img,
  };

}