import '../model/model_export.dart';

abstract class Repository{
  List<Product> findAllProducts();

  // Stream<List<Product>> watchAllProducts();

  int addProduct(Product product);

  void deleteProduct(Product product);

  Future init();

  void close();

}