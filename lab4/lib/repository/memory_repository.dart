import 'dart:async';
import 'dart:core';
import 'package:flutter/cupertino.dart';

import 'repository.dart';
import '../model/model_export.dart';

class MemoryRepository extends Repository with ChangeNotifier {
  final List<Product> _currentProduct = <Product>[];

  @override
  int addProduct(Product product) {
    _currentProduct.add(product);
    notifyListeners();

    return 0;
  }

  @override
  Future<void> deleteProduct(Product product) {
    _currentProduct.remove(product);
    notifyListeners();
    return Future.value();
  }

  @override
  List<Product> findAllProducts() {
    return _currentProduct;
  }

  @override
  Future init() {
    return Future.value();
  }

  @override
  void close() {
    _currentProduct.clear();
  }
}
